#include <iostream> //header file which include the standard functions
using namespace std; // using standard naming conventions

int main() //main(most important fn) starting point of any cpp program which will return integer type
{	
	int firstNum, secNum, sum; //firstno, secondno and sum integers to store the specific values
	cout<< "enter first and second no."; //output statement
	cin>>firstNum>>secNum; //inputting numbers
	sum=firstNum + secNum; //sum of two numbers
	cout<<"Sum of first and second no. is "<<sum; //output with sum
	return 0; //return 0 to function main
}