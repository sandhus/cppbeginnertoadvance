#include <iostream> //header file which include the standard functions
using namespace std; // using standard naming conventions

int main() //main(most important fn) starting point of any cpp program which will return integer type
{	
	int firstNum, secNum; //firstno, secondno and sum integers to store the specific values
	cout<< "enter first and second no."; //output statement
	cin>>firstNum>>secNum; //inputting numbers
	cout<<"Sum = "<<firstNum + secNum; //output with sum
	cout<<endl;
	cout<<"Average = "<<(firstNum + secNum)/2;
	cout<<endl;
	cout<<"Product = "<<firstNum * secNum;
	cout<<endl;
	
	cout<<"Large one =";
	if(firstNum >= secNum)
	cout<<firstNum;
	else
	cout<<secNum;
	
	cout<<endl;
	
	cout<<"Small one =";
	if(firstNum <= secNum)
	cout<<firstNum;
	else
	cout<<secNum;
	
	return 0; //return 0 to function main
}