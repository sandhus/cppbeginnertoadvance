#include <iostream> //header file which include the standard functions
using namespace std; // using standard naming conventions

int main() //main(most important fn) starting point of any cpp program which will return integer type
{	
	cout << static_cast<int> ('A') << endl;
	return 0;
}